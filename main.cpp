#include <iostream>
#include <vector>
#include <ctime>
#include "ttNode.h"

int main() {
    std::vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto *tree = new ttNode(vec[0]);
    for (int i = 1; i < 1000000; i++) {
        if (i != 1000) add(tree, i);
    }
    unsigned int start_time =  clock(); // начальное время
    add(tree, 1000);
    unsigned int end_time = clock(); // конечное время
    unsigned int search_time = end_time - start_time;
    std::cout << search_time << std::endl;

    unsigned int start_time1 =  clock(); // начальное время
    add(tree, 100000000);
    unsigned int end_time1 = clock(); // конечное время
    unsigned int search_time1 = end_time - start_time;
    std::cout << search_time << std::endl;
    return 0;
}