class Node:
    def __init__(self, data, parent=None):
        self.data = [data]
        self.parent = parent
        self.child = list()

    def _add(self, new_node):
        for child in new_node.child:
            child.parent = self
        self.data.extend(new_node.data)
        self.data.sort()
        if len(self.data) > 1:
            self.child.sort()
        if len(self.data) > 2:
            self._split()

    def _insert(self, new_node):
        if self._isleaf():
            self._add(new_node)

        elif new_node.data[0] > self.data[-1]:
            self.child[-1]._insert(new_node)
        else:
            for i in range(0, len(self.data)):
                if new_node.data[0] < self.data[i]:
                    self.child[i]._insert(new_node)
                    break

    def _split(self):
        left_side_child = Node(self.data[0], self)
        right_side_child = Node(self.data[2], self)
        if self.child:
            for x in range(0, 3):
                if x <= 1:
                    self.child[x].parent = left_side_child
                else:
                    self.child[x].parent = right_side_child

            left_side_child.child = [self.child[0], self.child[1]]
            right_side_child.child = [self.child[2], self.child[3]]

        self.child = [left_side_child, right_side_child]
        self.data = [self.data[1]]

        if self.parent:
            if self in self.parent.child:
                self.parent.child.remove(self)
            self.parent._add(self)

        else:
            left_side_child.parent = self
            right_side_child.parent = self

    def _isleaf(self):
        return len(self.child) == 0

    def _find(self, item):
        if item in self.data:
            return item
        elif self._isleaf():
            return False
        elif item > self.data[-1]:
            return self.child[-1]._find(item)
        else:
            for i in range(len(self.data)):
                if item < self.data[i]:
                    return self.child[i]._find(item)

    def _remove(self, item):
        if self.child:
            pra = self.child[-1]
            while pra.child:
                pra = pra.child[0]
            self.data[-1] = pra.data[0]
            pra.data = pra.data[1:]
            if not pra.data:
                pra.parent = None
        else:
            self.data = []


class Tree:
    def __init__(self):
        self.root = None

    def insert(self, item):
        if self.root is None:
            self.root = Node(item)
        else:
            self.root._insert(Node(item))
            while self.root.parent:
                self.root = self.root.parent
        return True

    def find(self, item):
        return self.root._find(item)

    def remove(self, item):
        self.root.remove(item)
