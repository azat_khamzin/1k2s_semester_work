from tree.py import Tree
from tree.py import Node
import time


def main():
    my_file = open("in_info.txt", "w+")

    
    for stroke in range(50):
        test_lst = []
        tree = Tree()
        str = list((my_file.readline(stroke + 1)).split(' '))
        for item in str:
            test_lst.append(int(item))
            
        start_cr = time.time()
        for item in test_lst:
            tree.insert(item)
        finish_cr = time.time()

        find_time_start = time.time()
        tree.find(test_lst[-5])
        find_time_finish = time.time()

        remove_time_start = time.time()
        tree.remove(test_lst[-7])
        remove_time_finish = time.time()

        cr_time = (finish_cr - start_cr) * 1000.0
        find_time = (find_time_finish - find_time_start) * 1000.0
        remove_time = (remove_time_finish - remove_time_start) * 1000.0

        print(cr_time, find_time, remove_time)
        del tree
        
        


if __name__ == "__main__":
    main()
