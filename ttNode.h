//
// Created by capkeik on 18.04.2021.
//

#ifndef INC_1K2S_SEMESTER_WORK_TTNODE_H
#define INC_1K2S_SEMESTER_WORK_TTNODE_H


struct ttNode {
private:
    int length;
    int key[3];
    ttNode *first;
    ttNode *second;
    ttNode *third;
    ttNode *fourth;
    ttNode *parent;

    static void swap(int &a, int &b) {
        int t = a;
        a = b;
        b = t;
    }

    static void sort(int &a) {
        return;
    }

    static void sort(int &a, int &b) {
        if (a > b) swap(a, b);
    }

    static void sort(int &a, int &b, int &c) {
        if (a > b) swap(a, b);
        if (a > c) swap(a, c);
        if (b > c) swap(b, c);
    }

    void sort() {
        if (length == 1) sort(key[0]);
        if (length == 2) sort(key[0], key[1]);
        if (length == 3) sort(key[0], key[1], key[2]);
    }


    void removeHelper(int t) {
        if (length == 2 && key[1] == t) {
            key[1] = key[2];
            length--;
        } else if (length >= 1 && key[0] == t) {
            key[0] = key[1];
            key[1] = key[2];
            length--;
        }
    }


    void addHelper(int t) {
        key[length] = t;
        length++;
    }

    bool isIn(int t) {
        for (int i = 0; i < length; i++) {
            if (key[i] == t) return true;
        }
        return false;
    }

    void become2Node(int t, ttNode *firstNew, ttNode *secondNew) {
        key[0] = t;
        first = firstNew;
        second = secondNew;
        third = nullptr;
        fourth = nullptr;
        parent = nullptr;
        length = 1;
    }

    bool isLeaf() {
        return (first == nullptr && second == nullptr && third == nullptr);
    }

public:
    explicit ttNode(int t) :
            length(0),
            key{t, 0, 0},
            first(nullptr),
            second(nullptr),
            third(nullptr),
            fourth(nullptr),
            parent(nullptr) {};


    ttNode(int t, ttNode *firstNew, ttNode *secondNew, ttNode *thirdNew, ttNode *fourthNew, ttNode *parentNew) :
            length(0),
            key{t, 0, 0},
            first(firstNew),
            second(secondNew),
            third(thirdNew),
            fourth(fourthNew),
            parent(parentNew) {};

    friend ttNode *add(ttNode *node, int k);

    friend ttNode *splitNode(ttNode *node);

    friend ttNode *search(ttNode *root, int k);

    friend ttNode *findMin(ttNode *node);

    friend ttNode *remove(ttNode *root, int k);

    friend ttNode *fix(ttNode *leaf);

    friend ttNode *mergeNodes(ttNode *leaf);

    friend ttNode *distribute(ttNode *leaf);
};

ttNode *add(ttNode *node, int k) {
    if (!node) return new ttNode(k);

    if (node->isLeaf()) node->addHelper(k);
    else if (k <= node->key[0]) add(node->first, k);
    else if (node->length == 1 || (node->length == 2 && k <= node->key[1])) add(node->first, k);
    else add(node->third, k);

    return splitNode(node);
}


ttNode *splitNode(ttNode *node) {
    if (node->length < 3) return node;

    auto *node1 = new ttNode(node->key[0],
                             node->first,
                             node->second,
                             nullptr,
                             nullptr,
                             node->parent);

    auto *node2 = new ttNode(node->key[2],
                             node->third,
                             node->fourth,
                             nullptr,
                             nullptr,
                             node->parent);

    if (node1->first) node1->first->parent = node1;
    if (node2->first) node2->first->parent = node2;
    if (node1->second) node1->second->parent = node1;
    if (node2->second) node2->second->parent = node2;

    if (node->parent) {
        node->parent->addHelper(node->key[1]);

        if (node->parent->first == node) {
            node->parent->fourth = node->parent->third;
            node->parent->third = node->parent->second;
            node->parent->second = node2;
            node->parent->first = node1;
        } else if (node->parent->second == node) {
            node->parent->fourth = node->parent->third;
            node->parent->third = node2;
            node->parent->second = node1;
        } else if (node->parent->third == node) {
            node->parent->fourth = node2;
            node->parent->third = node1;
        }

        ttNode *tmp = node->parent;
        delete node;
        return tmp;
    } else {
        node1->parent = node;
        node2->parent = node;
        node->become2Node(node->key[1], node1, node2);
        return node;
    }


}

ttNode *search(ttNode *root, int k) {
    if (root) {
        if (root->isIn(k)) return root;
        else if (k < root->key[0]) return search(root->first, k);
        else if (root->length == 2 && k < root->key[1]) return search(root->second, k);
        else if (root->length == 2) return search(root->third, k);
    }
}

ttNode *findMin(ttNode *node) {
    if (!node) return node;
    if (!(node->fourth)) return node;
    else return findMin(node->first);
}

ttNode *remove(ttNode *root, int k) {
    ttNode *node = search(root, k);

    if (!node) return root;

    ttNode *min = nullptr;
    if (node->key[0] == k) min = findMin(node->second);
    else min = findMin(node->third);

    if (min) {
        int &c = (k == node->key[0] ? node->key[0] : node->key[1]);
        ttNode::swap(c, min->key[0]);
        node = min;
    }

    node->removeHelper(k);
    return fix(node);
}

ttNode *fix(ttNode *leaf) {
    if (leaf->length == 0 && leaf->parent == nullptr) {
        delete leaf;
        return nullptr;
    }
    ttNode *parent = leaf->parent;
    if (leaf->length != 0) {
        if (parent) return fix(parent);
        else return leaf;
    }
    if (parent->first->length == 2 || parent->second->length == 2 || parent->length == 2) leaf = distribute(leaf);
    else if (parent->length == 2 && parent->third->length == 2);
    else leaf = mergeNodes(leaf);

    return fix(leaf);
}


#endif //INC_1K2S_SEMESTER_WORK_TTNODE_H
